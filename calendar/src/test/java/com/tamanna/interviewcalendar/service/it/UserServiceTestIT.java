package com.tamanna.interviewcalendar.service.it;

import com.tamanna.interviewcalendar.repository.UserRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import java.util.Locale;

public class UserServiceTestIT extends AbstractIT{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    @Test
    public void test() {
        var result = userRepository.findAll();
        Assert.assertEquals(2, result.size());
        Assert.assertEquals("Credit Card",  messageSource.getMessage("CREDIT_CARD", null, new Locale("en_US")));
    }
}
