package com.tamanna.interviewcalendar.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Car {
    private String brand;
    private Double price;
}
