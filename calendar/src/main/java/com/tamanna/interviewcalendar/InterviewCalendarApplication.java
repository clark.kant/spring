package com.tamanna.interviewcalendar;

import com.tamanna.interviewcalendar.bean.Car;
import com.tamanna.interviewcalendar.config.ProjectConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class InterviewCalendarApplication {

    public static void main(String[] args) {
        var ano = new AnnotationConfigApplicationContext(ProjectConfig.class);
        var bean = ano.getBean(Car.class);
        System.out.println(bean.getBrand());

        SpringApplication.run(InterviewCalendarApplication.class, args
        );
    }

}
